import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay


X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

# a)
plt.figure()
plt.scatter(x=X_train[:,0], y=X_train[:,1], c=y_train)
plt.scatter(x=X_test[:,0], y=X_test[:,1], c=y_test, marker='x')
plt.show()

# b)
logReg_model = LogisticRegression()
logReg_model.fit(X_train, y_train)

# c)
w0 = logReg_model.intercept_[0]
w1, w2 = logReg_model.coef_.T
b = -w0/w2
a = -w1/w2

# x2 = b + a*x1
x_min, x_max = -4, 4
y_min, y_max = -4, 4
xd = np.array([x_min, x_max])
yd = b + a*xd

plt.figure()
plt.scatter(x=X_train[:,0], y=X_train[:,1], c=y_train)
plt.plot(xd, yd)
plt.show()

# d)
y_test_p = logReg_model.predict(X_test)
cfMat = confusion_matrix(y_test, y_test_p)
disp = ConfusionMatrixDisplay(cfMat)
disp.plot()
plt.show()

acc = (cfMat[0, 0]+cfMat[1, 1])/(cfMat[0, 0]+cfMat[0, 1]+cfMat[1, 0]+cfMat[1, 1])
prc = cfMat[0, 0]/(cfMat[0, 0]+cfMat[1, 0])
rcl = cfMat[0, 0]/(cfMat[0, 0]+cfMat[0, 1])
print(f"Točnost: {acc}\nPreciznost: {prc}\nOdziv: {rcl}")

# e)
trueRes = []
falseRes = []
for i in range(len(y_test)):
    if y_test[i] == y_test_p[i]:
        trueRes.append([X_test[i, 0], X_test[i, 1]])
    else:
        falseRes.append([X_test[i, 0], X_test[i, 1]])
trueRes = np.array(trueRes)
falseRes = np.array(falseRes)
plt.figure()
plt.scatter(x=trueRes[:,0], y=trueRes[:,1], c='#00ff00')
plt.scatter(x=falseRes[:,0], y=falseRes[:,1], c='#000000')
plt.show()