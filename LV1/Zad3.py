# Zadatak 3
numbers = []
while True:
    num = input()
    if num == "Done":
        break
    try:
        numbers.append(int(num))
    except:
        print("Not a valid numbger")

numbers.sort()
sum = 0
for number in numbers:
    sum += number
print(f"Length: {len(numbers)}")
print(f"Average: {sum/len(numbers)}")
print(f"Min: {min(numbers)}")
print(f"Max: {max(numbers)}")
print(f"Sorted list: {numbers}")