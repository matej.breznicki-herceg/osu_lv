# Zadatak 2
try:
    score = float(input())
    if score > 1 or score < 0:
        print("Number is not in interval [0.0, 1.0]")
    else:
        if score >= 0.9:
            grade = 'A'
        elif score >= 0.8:
            grade = 'B'
        elif score >= 0.7:
            grade = 'C'
        elif score >= 0.6:
            grade = 'D'
        else:
            grade = 'F'
        print(grade)
except:
    print("Enter a number")