# Zadatak 1
def total_euro(hours, paidPerHour):
    return hours * paidPerHour

print("Radni sati:")
hours = int(input())

print("euro/h:")
paidPerHour = float(input())
pay = total_euro(hours, paidPerHour)
print(f"Ukupno: {pay} eura")