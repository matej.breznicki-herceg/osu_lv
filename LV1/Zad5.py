# Zadatak 5
def calculateAverageWordNumber(listOfLines):
    sum = 0
    for line in listOfLines:
        sum += len(line.split())-1
    return int(sum/len(listOfLines))

def countLinesEndingWith(listOfLines, simbol):
    i = 0
    for line in listOfLines:
        if line[len(line)-2] == simbol:
            i = i + 1
    return i

fhead = open('SMSSpamCollection.txt')
hamList = []
spamList = []
for line in fhead:
    if line.__contains__('ham'):
        hamList.append(line)
    else:
        spamList.append(line)
print(f"Average word number in ham messages: {calculateAverageWordNumber(hamList)}")
print(f"Average word number in spam messages: {calculateAverageWordNumber(spamList)}")
print(f"There are {countLinesEndingWith(spamList, '!')} words ending with !")
fhead.close()