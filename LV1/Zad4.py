# Zadatak 4
fhand = open('song.txt')
wordDict = {}
for line in fhand:
    words = line.split()
    for word in words:
        word = word.lower()
        try:
            wordDict[word] = wordDict[word] + 1
        except:
            wordDict[word] = 1
mentionedOnce = []
for word in wordDict.keys():
    if wordDict[word] == 1:
        mentionedOnce.append(word)
print(f"{len(mentionedOnce)} words were mentione once:")
print(f"{mentionedOnce}")
fhand.close()