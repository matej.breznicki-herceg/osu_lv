import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from keras.models import load_model

import os

path = os.path.join(os.path.dirname(__file__), "FCN/")

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
model = load_model(path)

x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

x_train_2= x_train_s.reshape(60000, 784)
x_test_2= x_test_s.reshape(10000, 784)

y_predicted = model.predict(x_test_2)
y_p = []

for i in range(len(y_test)):
  temp = (y_predicted[i,:] == y_predicted[i,:].max())
  index = np.where(temp)
  y_p.append(index)

y_p = np.array(y_p)
y_p = y_p.reshape(10000)

wrongs = (y_p != y_test)
i = np.where(wrongs)
i = np.array(i)

index = i[:,:3]

for ind in index[0,:]:
  plt.figure()
  plt.imshow(x_test[ind])
  plt.title(f"Očekivano: {y_test[ind]} Predviđeno: {y_p[ind]}")
  plt.show()