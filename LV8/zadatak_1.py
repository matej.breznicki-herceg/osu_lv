import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix

import os

# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa

# 1.
print(f"Skup za učenje: {len(x_train) + len(y_train)}")
print(f"Skup za testiranje: {len(x_test) + len(y_test)}")

# Slike su skalirane od 0 do 1 kao float tip podatka, izlazna veličina je kodirana kao vektor sa nulama i jednom jedinicom (1-od-K kodiranje)

plt.figure()
plt.imshow(x_train[4])
plt.title("Slika 5")
plt.show()
print(f"Oznaka slike: {y_train[4]}")

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model = keras.Sequential()
model.add(layers.Input(shape=(784)))
model.add(layers.Dense(100, activation="relu"))
model.add(layers.Dense(50, activation="relu"))
model.add(layers.Dense(10, activation="sigmoid"))

model.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model.compile(
    loss="categorical_crossentropy",
    optimizer="adam",
    metrics=["accuracy"]
)


# TODO: provedi ucenje mreze
x_train_2= x_train_s.reshape(60000, 784)
x_test_2= x_test_s.reshape(10000, 784)

batch_size = 32
epochs = 2
history = model.fit(x_train_2, y_train_s, batch_size=batch_size, epochs=epochs)


# TODO: Prikazi test accuracy i matricu zabune
score = model.evaluate(x_test_2, y_test_s, verbose=0)
print(score)

y_predicted = model.predict(x_test_2)
y_p = []

for i in range(len(y_test)):
  temp = (y_predicted[i,:] == y_predicted[i,:].max())
  index = np.where(temp)
  y_p.append(index)

y_p = np.array(y_p)
y_p = y_p.reshape(10000)
cfMat = confusion_matrix(y_test, y_p)
disp = ConfusionMatrixDisplay(cfMat)
disp.plot()
plt.show()

# TODO: spremi model
path = os.path.join(os.path.dirname(__file__), "FCN/")
model.save(path)