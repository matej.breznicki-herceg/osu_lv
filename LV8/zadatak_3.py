import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from matplotlib import image as Image
from sklearn.metrics import confusion_matrix
from keras . models import load_model

import os

model = load_model("FCN/")

path = os.path.join(os.path.dirname(__file__), "test/7.png")
img = Image.imread(path)

img = img[:,:,0]
w,h = img.shape

wSlice = int(w/28)
hSlice = int(h/28)

new_w = wSlice * 28
new_h = hSlice * 28

img = img[:new_w,:new_h]
img_28x28 = []
w_start = 0
h_start = 0
for j in range(28):
  temp = []
  w_start = 0
  for i in range(28):
    block = img[w_start:w_start+wSlice, h_start:h_start+hSlice]
    w_start = w_start+wSlice
    temp.append(block.max())
  img_28x28.append(temp)
  h_start = h_start+hSlice

img = np.array(img_28x28)
img = img.transpose()
img_s = img.astype("float32") / 255
img_s = np.expand_dims(img_s, -1)

plt.figure()
plt.imshow(img_s)
plt.show()

img_2= img_s.reshape(1, 784)
y_predicted = model.predict(img_2)

temp = (y_predicted[:] == y_predicted[:].max())
temp, index = np.where(temp)

print(index)

# Za svaku sliku ispisuje da je broj na slici 5, ali broj na slici nije uvijek 5
# Razlog toga je vjerovatno overfitanje