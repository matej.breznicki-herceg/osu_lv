import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

import os

path = os.path.join(os.path.dirname(__file__), "imgs/test_1.jpg")

# ucitaj sliku
img = Image.imread(path)

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

# 1.
# Ima 106276 boja

# 2.
km = KMeans(n_clusters=5, n_init=5)
km.fit(img_array_aprox)
res = km.predict(img_array_aprox)

# 3.
centers = km.cluster_centers_
if d > 3:
    centers = centers[:,0:3] * 1000
    d = 3
img_array_aprox = centers[res]
imgMin = np.reshape(img_array_aprox, (w, h, d))


# 4.
plt.figure()
plt.title("Minimizirana slika")
plt.imshow(imgMin)
plt.tight_layout()
plt.show()

# Promjenom K mijenja se količina boja na slici, ako je K 2 onda ima samo dvije boje, a ako je K 10 onda slika ima 10 boja

# 5.
# Na svim slikama program radi (kod testiranja samo se importala druga slika, nema dodatnog koda)

# 6.
inertia = []
for i in range(2, 9):
  km.n_clusters = i
  km.fit(test)
  inertia.append(km.inertia_)
plt.plot(range(2, 9), inertia)
plt.ylabel("J")
plt.xlabel("K")
plt.title("J/K graf")
plt.show()

# Moguće je uočiti lakat koji upućuje na optimalan broj grupa

# 7.
imgMin[imgMin[:,:] != centers[1]] = 1
plt.figure()
plt.title("Izdvojen samo jedan element slike")
plt.imshow(imgMin)
plt.tight_layout()
plt.show()

# Izdvojen je samo jedan element iz slike (samo onaj dio slike koji ima istu boju)