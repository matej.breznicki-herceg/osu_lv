import numpy as np
import matplotlib.pyplot as plt
import os

path = os.path.join(os.path.dirname(__file__), 'data.csv')
data = np.loadtxt(path, skiprows=1, delimiter=',')
print(f"Mjerenje izvršeno nad {int(data.size/3)} osoba")
height = data[:,1]
weight = data[:,2]
plt.scatter(x=height, y=weight, c='b', linewidths=1)
plt.xlabel('Height')
plt.ylabel('Weight')
plt.show()
plt.scatter(x=height[::50], y=weight[::50], c='b', linewidths=1)
plt.xlabel('Height')
plt.ylabel('Weight')
plt.show()
print(f"Min visina: {height.min()} Max visina: {height.max()} Srednja visina: {height.mean()}")
ind = (data[:,0] == 1)
numberOfMales = 0
for i in ind:
    if(i):
        numberOfMales += 1
print(f"Min muška visina: {data[:numberOfMales,1].min()} Max muška visina: {data[:numberOfMales,1].max()} Srednja muška visina: {data[:numberOfMales,1].mean()}")
print(f"Min ženska visina: {data[numberOfMales:,1].min()} Max ženska visina: {data[numberOfMales:,1].max()} Srednja ženska visina: {data[numberOfMales:,1].mean()}")