from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_absolute_percentage_error, r2_score
import sklearn.linear_model as lm
import pandas as pd
import matplotlib.pyplot as plt
import os
from math import sqrt

path = os.path.join(os.path.dirname(__file__), 'data_C02_emission.csv')
data = pd.read_csv(path)

# a)
x_data = data[['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']]
y_data = data['CO2 Emissions (g/km)']
x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.2, random_state=1)

# b)
# Engine Size
plt.figure()
plt.scatter(x=x_train['Engine Size (L)'], y=y_train, c='blue')
plt.scatter(x=x_test['Engine Size (L)'], y=y_test, c='red')
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()
# Cylinders
plt.figure()
plt.scatter(x=x_train['Cylinders'], y=y_train, c='blue')
plt.scatter(x=x_test['Cylinders'], y=y_test, c='red')
plt.xlabel('Cylinders')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()
# Fuel Consumption City
plt.figure()
plt.scatter(x=x_train['Fuel Consumption City (L/100km)'], y=y_train, c='blue')
plt.scatter(x=x_test['Fuel Consumption City (L/100km)'], y=y_test, c='red')
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()
# Fuel Consumption Hwy
plt.figure()
plt.scatter(x=x_train['Fuel Consumption Hwy (L/100km)'], y=y_train, c='blue')
plt.scatter(x=x_test['Fuel Consumption Hwy (L/100km)'], y=y_test, c='red')
plt.xlabel('Fuel Consumption Hwy (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()
# Fuel Consumption Comb (L/100km)
plt.figure()
plt.scatter(x=x_train['Fuel Consumption Comb (L/100km)'], y=y_train, c='blue')
plt.scatter(x=x_test['Fuel Consumption Comb (L/100km)'], y=y_test, c='red')
plt.xlabel('Fuel Consumption Comb (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()
# Fuel Consumption Comb (mpg)
plt.figure()
plt.scatter(x=x_train['Fuel Consumption Comb (mpg)'], y=y_train, c='blue')
plt.scatter(x=x_test['Fuel Consumption Comb (mpg)'], y=y_test, c='red')
plt.xlabel('Fuel Consumption Comb (mpg)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

# c)
stdSc = StandardScaler()
x_train_n = pd.DataFrame(stdSc.fit_transform(x_train), columns=['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)'])
x_test_n = pd.DataFrame(stdSc.transform(x_test), columns=['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)'])
fig, ax = plt.subplots(2)
ax[0].hist(x_train['Fuel Consumption Comb (L/100km)'])
ax[0].set_title('Not scaled')
ax[1].hist(x_train_n['Fuel Consumption Comb (L/100km)'])
ax[1].set_title('Scaled')
plt.show()

# d)
linearModel = lm.LinearRegression()
linearModel.fit(x_train_n, y_train)
print(f"Parametri: \n{linearModel.coef_}")

# e)
y_test_p = linearModel.predict(x_test_n)
plt.figure()
plt.scatter(x=x_test['Engine Size (L)'], y=y_test_p, c='blue')
plt.scatter(x=x_test['Engine Size (L)'], y=y_test, c='red')
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.show()

# f)
MSE = mean_squared_error(y_test, y_test_p)
RMSE = sqrt(MSE)
MAE = mean_absolute_error(y_test, y_test_p)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)
print(f"MSE: {MSE}")
print(f"RMSE: {RMSE}")
print(f"MAE: {MAE}")
print(f"MAPE: {MAPE}")
print(f"R2: {R2}")

# g)

# Smanjenjem broja ulaznih veličina evaluacijske vrijednosti rastu