from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_absolute_percentage_error, r2_score
from sklearn.compose import ColumnTransformer
from sklearn.compose import make_column_selector as selector
from sklearn.pipeline import make_pipeline
import sklearn.linear_model as lm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt
import os

path = os.path.join(os.path.dirname(__file__), 'data_C02_emission.csv')
data = pd.read_csv(path)

x_data = data[['Engine Size (L)', 'Cylinders', 'Fuel Type', 'Fuel Consumption City (L/100km)', 'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)', 'Fuel Consumption Comb (mpg)']]
y_data = data['CO2 Emissions (g/km)']


num_selector = selector(dtype_exclude=object)
ctg_selector = selector(dtype_include=object)
num_columns = num_selector(x_data)
ctg_columns = ctg_selector(x_data)
ohe = OneHotEncoder ()
stdSc = StandardScaler()

preprocessor = ColumnTransformer(
    [
        ("one-hot-encoder", ohe, ctg_columns),
        ("standard_scaler", stdSc, num_columns),
    ]
)

linearModel = make_pipeline(preprocessor, lm.LinearRegression())

x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.2, random_state=1)

linearModel.fit(x_train, y_train)

y_test_p = linearModel.predict(x_test)
MSE = mean_squared_error(y_test, y_test_p)
RMSE = sqrt(MSE)
MAE = mean_absolute_error(y_test, y_test_p)
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
R2 = r2_score(y_test, y_test_p)
print(f"MSE: {MSE}")
print(f"RMSE: {RMSE}")
print(f"MAE: {MAE}")
print(f"MAPE: {MAPE}")
print(f"R2: {R2}")

# Maksimalna pogreška u procijeni

err = []

for i in range(len(y_test)):
  err.append(abs(np.array(y_test)[i] - np.array(y_test_p)[i]))

err = np.array(err)
max_err = err.max()
max_err_car = data[data.index == err.argmax()]
print(f"Maximalna pogreška u procijeni: \n{max_err} \nAutomobil: \n{max_err_car[['Make', 'Model']]}")