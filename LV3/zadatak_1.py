import pandas as pd
import os

path = os.path.join(os.path.dirname(__file__), 'data_C02_emission.csv')
data = pd.read_csv(path)

# a)
print("\na)\n")
print(f"Broj mjerenja: {len(data)}")
print(data.info())
if data.isnull().sum().sum() <= 0 or data.duplicated().sum() <= 0:
    print("Nema dupliciranih vrijednosti")
else:
    print("Ima dupliciranih vrijednosti")
    data.dropna(axis=0)
    data.drop_duplicates()
    data = data.reset_index(drop=True)
data["Make"] = data["Make"].astype("category")
data["Model"] = data["Model"].astype("category")
data["Vehicle Class"] = data["Vehicle Class"].astype("category")
data["Transmission"] = data["Transmission"].astype("category")
data["Fuel Type"] = data["Fuel Type"].astype("category")

# b)
print("\nb)\n")
data = data.sort_values(by=['Fuel Consumption City (L/100km)'])
print(f"Najmanja potrošnja: \n{data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].head(3)}")
print(f"Najveća potrošnja: \n{data[['Make', 'Model', 'Fuel Consumption City (L/100km)']].tail(3)}")

# c)
print("\nc)\n")
cars = data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print(f"Broj automobila s veličinom motora između 2.5 i 3.5L: {len(cars)}")
print(f"Prosječna CO2 emisija tih automobila: {cars['CO2 Emissions (g/km)'].mean()}")

# d)
print("\nd)\n")
audis = data[data['Make'] == 'Audi']
print(f"Mjerenja vozila proizvođača Audi: {len(audis)}")
print(f"Prosječna emisija CO2 za Audije s 4 cilindra: {audis[audis['Cylinders'] == 4]['CO2 Emissions (g/km)'].mean()}")

# e)
print("\ne)\n")
cylinders = data['Cylinders'].drop_duplicates()
for cylinder in cylinders:
    cars = data[data['Cylinders'] == cylinder]
    print(f"Auti s {cylinder} cilindara: {len(cars)}")
    print(f"Prosječna CO2 emisija auta s {cylinder}: {cars['CO2 Emissions (g/km)'].mean()}")

# f)
print("\nf)\n")
diesel = data[data['Fuel Type'] == 'D']
gasoline = data[(data['Fuel Type'] == 'X') | (data['Fuel Type'] == 'Z')]
print(f"Prosječna gradska potrošnja za benzince: {gasoline['Fuel Consumption City (L/100km)'].mean()} median: {gasoline['Fuel Consumption City (L/100km)'].median()}")
print(f"Prosječna gradska potrošnja za dizelaše: {diesel['Fuel Consumption City (L/100km)'].mean()} median: {diesel['Fuel Consumption City (L/100km)'].median()}")

# g)
print("\ng)\n")
cars = data[(data['Fuel Type'] == 'D') & (data['Cylinders'] == 4)]
car = cars.sort_values(by=['Fuel Consumption City (L/100km)']).tail(1)
print(f"Vozilo s 4 cilindra i dizelskim motorom koje ima največu gradsku potrošnju: \n{car[['Make', 'Model']]}")

# h)
print("\nh)\n")
man = data['Transmission'].apply(lambda x:x.__contains__('M'))
manual = data[man]
print(f"Broj automobila s ručnim mjenjačem: {len(manual)}")

# i)
print("\ni)\n")
print(data.corr(numeric_only=True))