import pandas as pd
import matplotlib as plt
import os

path = os.path.join(os.path.dirname(__file__), 'data_C02_emission.csv')
data = pd.read_csv(path)
data["Make"] = data["Make"].astype("category")
data["Model"] = data["Model"].astype("category")
data["Vehicle Class"] = data["Vehicle Class"].astype("category")
data["Transmission"] = data["Transmission"].astype("category")
data["Fuel Type"] = data["Fuel Type"].astype("category")

# a)
plt.figure()
data['CO2 Emissions (g/km)'].plot(kind='hist', bins=20)

# b)
plt.figure()
data.plot.scatter(x='Fuel Consumption City (L/100km)', y='CO2 Emissions (g/km)', c='Fuel Type', colormap="viridis")
plt.show()

# c)
plt.figure()
data.boxplot(column=['Fuel Consumption Hwy (L/100km)'], by='Fuel Type')
plt.show()

# d)
plt.figure()
data.groupby('Fuel Type').count().plot.bar(color="blue").legend().remove()
plt.show()

# e)
data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean().plot.bar(color="blue")
plt.show()